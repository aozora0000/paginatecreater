<?php
/*
	ページネーション生成クラス
	$paginate = new Paginate; //インスタンス生成
	//ページネーションオプション
	active/prev/next	:	エレメントのクラス設定
	sign				:	次・前のリンク（指定無しで非表示）
	$paginate_option = array(
		"active"=>"active",
		"prev"=>"prev",
		"next"=>"next",
		"sign"=>array(
			"prev"=>"前へ",
			"next"=>"次へ"
		)
	);
	//
	print $paginate->create("現在ページ","最大ページ","ページネーション表示数","リンクのラッパーエレメント","ラッパーエレメントのクラス指定","GETパラメータ",$paginate_option);
*/
	class Paginate3 {
		public $paginate;
		public function __constract() {
			$this->paginate = "";
		}

		public function create(
			$page,					//現在のページ数
			$count,					//最大ページ数 ceil(１ページ表示数/取得件数)
			$paginate_limit = 10,	//ページネーション表示数
			$element = "li",		//ページネーション表示エレメント
			$class = "",			//クラス付与
			$get_param = "p",		//リンク用GETパラメータ設定
			$option = array(
				"active"=>"",
				"prev"=>"",
				"next"=>"",
				"sign"=>array(
					"next"=>"",
					"prev"=>""
				)
			)
		) {
			$class = ($class == "") ? "": $class." ";
			$this->page = $page;
			$this->count = $count;
			$this->paginate_limit = $paginate_limit;

			//ページ設定
			$this->calculate($this->page,$this->count,$this->paginate_limit);
			if($option["sign"]["prev"] != "") {
				if($page - 1 > 0) {
					self::add_sign(
						$element,
						$page - 1,
						$class.$option["prev"],
						$get_param,
						$option["sign"]["prev"]
					);
				} else {
					self::add_sign(
						$element,
						$page - 1,
						$class.$option["prev"]." active",
						"",
						$option["sign"]["prev"]
					);
				}
			}
			for($i = $this->start_page;$i <= $this->end_page;$i++) {
				if($i != 0) {
					if($i == $page) {
						self::add($element,$i,$class.$option["active"],"");
					} elseif($i + 1 == $page) {
						self::add($element,$i,$class.$option["prev"],$get_param);
					} elseif($i - 1 == $page) {
						self::add($element,$i,$class.$option["next"],$get_param);
					} else {
						self::add($element,$i,$class,$get_param);
					}
				}
			}
			if($option["sign"]["next"] != "") {
				if($page + 1 <= $count) {
					self::add_sign(
						$element,
						$page + 1,
						$class.$option["next"],
						$get_param,
						$option["sign"]["next"]
					);
				} else {
					self::add_sign(
						$element,
						$page + 1,
						$class.$option["next"]." active",
						"",
						$option["sign"]["next"]
					);
				}
			}
			return $this->paginate;
		}


		private function add_sign(
			$element,
			$page,
			$class,
			$get_param,
			$char
		) {
			//classが無い時
			if($class == "") {
				if($get_param == "") {
					$this->paginate .= sprintf('<%s>%s</%s>',$element,$char,$element);
				} else {
					$this->paginate .= sprintf('<%s><a href="?%s=%s">%s</a></%s>',$element,$get_param,$page,$char,$element);
				}
			} else {
				if($get_param == "") {
					$this->paginate .= sprintf('<%s class="%s">%s</%s>',$element,$class,$char,$element);
				} else {
					$this->paginate .= sprintf('<%s class="%s"><a href="?%s=%s">%s</a></%s>',$element,$class,$get_param,$page,$char,$element);
				}
			}
		}

		private function add($element,$page,$class,$get_param) {
			//classが無い時
			if($class == "") {
				if($get_param == "") {
					$this->paginate .= sprintf('<%s>%s</%s>',$element,$page,$element);
				} else {
					$this->paginate .= sprintf('<%s><a href="?%s=%s">%s</a></%s>',$element,$get_param,$page,$page,$element);
				}
			} else {
				if($get_param == "") {
					$this->paginate .= sprintf('<%s class="%s">%s</%s>',$element,$class,$page,$element);
				} else {
					$this->paginate .= sprintf('<%s class="%s"><a href="?%s=%s">%s</a></%s>',$element,$class,$get_param,$page,$page,$element);
				}
			}
		}

		private function calculate($page,$count,$paginate_limit) {

			if($paginate_limit > $count) {
				//最大ページ数がページネート個数より小さい場合
				$this->pattern = 4;
				$this->start_page = 1;
				$this->end_page = $count;
			} elseif ($paginate_limit / 2 >= $page) {
				//現在ページ番号がページネート個数の半分より小さい場合
				$this->pattern = 1;
				$this->start_page = 1;
				$this->end_page = $paginate_limit;
			} elseif($page + floor($paginate_limit/2) - 1 >= $count) {
				//最大ページ数が現在ページ数+ページネート個数を上回った場合
				$this->pattern = 3;
				$this->start_page = $count - $paginate_limit + 1;
				$this->end_page = $count;
			} else {
				//それ以外の場合
				$this->pattern = 2;
				$this->start_page = $page - floor($paginate_limit / 2);
				$this->end_page = $page + floor($paginate_limit / 2)-1;
			}
		}
	}